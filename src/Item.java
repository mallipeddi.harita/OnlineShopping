import java.util.List;


public class Item {
    private int brandCode;
    private String brandName;
    private float price;
    private ItemType itemType;

    public int getBrandCode() {
        return brandCode;
    }

    public String getBrandName() {
        return brandName;
    }

    public float getPrice() {
        return price;
    }

    public ItemType getItemType() {
        return itemType;
    }



    public void setBrandCode(int brandCode) {
        this.brandCode = brandCode;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setItemType(ItemType itemType) {
        if(itemType == ItemType.CLOTHES)
            this.itemType = ItemType.CLOTHES;
        else  if(itemType == ItemType.ACCESSORIES)
            this.itemType = ItemType.ACCESSORIES;
    }


    public Item(int brandCode, String brandName, float price, ItemType itemType)
    {
        this.setBrandCode(brandCode);
        this.setBrandName(brandName);
        this.setPrice(price);
        this.setItemType(itemType);
    }

    public void viewAllItems(List<Item> allItems)
    {

        allItems.stream().forEach(p -> System.out.println(p));

    }



}
