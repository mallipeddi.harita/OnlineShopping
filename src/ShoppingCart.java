import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart {

    private Item item;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public ShoppingCart(int brandCode, String brandName, float price, ItemType itemType) {
       Item item = new Item(brandCode,brandName,price,itemType);
       this.setItem(item);
        //for now hard coded as owner:wq


    }

    public List<ShoppingCart> addItemtoCart(int brandCode, String brandName, float price, ItemType itemType,
                                            List<ShoppingCart> cart) {
        cart.add(new ShoppingCart(brandCode, brandName, price, itemType));
        return cart;
    }

    public List<ShoppingCart> removeItemFromCart(int brandCode, List<ShoppingCart> cart) {
        return cart.stream().dropWhile(i -> i.getItem().getBrandCode() == brandCode).collect(Collectors.toList());
    }

    public void viewAllCartItems(List<ShoppingCart> allcartItems) {

        allcartItems.stream().forEach(p -> System.out.println(p));

    }
}
