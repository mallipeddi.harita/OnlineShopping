import java.util.List;
import java.util.Scanner;

public class Person {

    private String firstName;
    private String lastName;
    private String address;
    private UserType userType;
    private ShoppingCart shoppingCart;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public UserType getUserType() {
        return userType;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setUserType(UserType userType) {
        if(userType == UserType.OWNER)
        this.userType = UserType.OWNER;
        if(userType == UserType.CONSUMER)
            this.userType = UserType.CONSUMER;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public Person()
{
    this("Haritha","Mallipeddi","HYD",UserType.OWNER,null);
}

    public Person(String firstName, String lastName, String address, UserType userType,ShoppingCart shoppingCart)
    {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setAddress(address);
        this.setUserType(userType);
        this.setShoppingCart(shoppingCart);
    }
    void login(UserType userType){
        if(userType == UserType.OWNER)
        {
            System.out.println("Welcome Owner" + this.getFirstName() + " "  + this.getLastName());
        }
        else if(userType == UserType.CONSUMER) {
            System.out.println("Welcome Consumer" + this.getFirstName() + " "  + this.getLastName());
        }
    }



    public String Logout() {
        return "You have logged out!";
    }

}